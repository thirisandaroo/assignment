<?php

//Refactor The Code

$vehicleTypes = ['sport-car', 'truck', 'bike', 'boat'];
$vehiclesSpeed = [150, 60, 100, 50]; // vehicles speed in km/h
$distance = 350; // destination distance in km

echo("Duration of each vehicle to reach destination\n");

foreach ($vehicleTypes as $index => $vehicleType) {
    $speed = $distance / $vehiclesSpeed[$index];

    if ($vehicleType == 'boat') {
        echo($vehicleType . ": " . ($speed + 0.25));
    } else {
        echo($vehicleType . ": ". $speed);
    }
}