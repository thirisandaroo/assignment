<?php

//Write a PHP program that pushes half of the zeros in a given array to the beginning of the array, and the other half to the end

function transformedArray(array $inputArray) {

    $zeroArray = [];

    foreach ($inputArray as $index => $value) {
        if ($value === 0) {
            $zeroArray [] = $value;
            unset($inputArray[$index]);
        }
    }

    $zeroArrayLength = count($zeroArray);

    $middle = (int)(round($zeroArrayLength / 2));

    $firstZeroArray = array_fill(0, $middle, 0);
    $lastZeroArray = array_fill(0, $zeroArrayLength - $middle, 0);

    return array_merge($firstZeroArray, $inputArray, $lastZeroArray);
}

